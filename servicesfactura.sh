#!/bin/bash
#autor: Alexander Sandoval
#fecha: 30/03/2022
#Descripcion: script que valida todos los servicios de factura e indica cual esta abajo
#Modificacion: 07/06/2022


echo "Servicios"
echo
echo "inicioAceptacionAutomatica.service"
sudo systemctl status inicioAceptacionAutomatica.service |  egrep 'failed|inactive'
echo

echo "inicioAceptacion.service"
sudo systemctl status inicioAceptacion.service |  egrep 'failed|inactive'
echo

echo "inicioBackDelFront.service"
sudo systemctl status inicioBackDelFront.service |  egrep 'failed|inactive'
echo

echo "inicioCargaMasiva.service"
sudo systemctl status inicioCargaMasiva.service |  egrep 'failed|inactive'
echo

echo "inicioConsultaDocumentos.service"
sudo systemctl status inicioConsultaDocumentos.service |  egrep 'failed|inactive'
echo

echo "inicioConsultaEstado.service"
sudo systemctl status inicioConsultaEstado.service | egrep 'failed|inactive'
echo

echo "inicioEmisionRecepcion.service"
sudo systemctl status inicioEmisionRecepcion.service |  egrep 'failed|inactive'
echo

echo "inicioInformacionFacturador.service"
sudo systemctl status inicioInformacionFacturador.service |  egrep 'failed|inactive'
echo

echo "inicioNotificacionAdquiriente.service"
sudo systemctl status inicioNotificacionAdquiriente.service |  egrep 'failed|inactive'
echo

echo "inicioNotificador.service"
sudo systemctl status inicioNotificador.service |  egrep 'failed|inactive'
echo

echo "inicioPingDian.service"
sudo systemctl status inicioPingDian.service |  egrep 'failed|inactive'
echo

echo "inicioProcesamiento.service"
sudo systemctl status inicioProcesamiento.service |  egrep 'failed|inactive'
echo

echo "inicioRecepcionDocumentos.service"
sudo systemctl status inicioRecepcionDocumentos.service |  egrep 'failed|inactive'
echo

echo "inicioPingDian.service"
sudo systemctl status inicioPingDian.service |  egrep 'failed|inactive'
echo

echo "inicioReenvioDian.service"
sudo systemctl status inicioReenvioDian.service |  egrep 'failed|inactive'
echo

echo "inicioRegeneradorPdf.service"
sudo systemctl status inicioRegeneradorPdf.service |  egrep 'failed|inactive'
echo

echo "inicioSeguridad.service"
sudo systemctl status inicioSeguridad.service |  egrep 'failed|inactive'
echo

echo "inicioWupplier.service"
sudo systemctl status inicioSeguridad.service |  egrep 'failed|inactive'
echo
